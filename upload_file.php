<?php
include_once('config.php');

if(!isset($_SESSION)){ 	session_start(); }
IF (!isset($_SESSION['Username'])) { header('Location: "'. $homepage); }
	
$allowedExts = array("jpg", "jpeg", "gif", "png");
$extension = end(explode(".", $_FILES["file"]["name"]));
if ((($_FILES["file"]["type"] == "image/gif")
|| ($_FILES["file"]["type"] == "image/jpeg")
|| ($_FILES["file"]["type"] == "image/png")
|| ($_FILES["file"]["type"] == "image/pjpeg"))
&& ($_FILES["file"]["size"] < 50000)
&& in_array($extension, $allowedExts))
  {
  if ($_FILES["file"]["error"] > 0)
    { echo "Error upload: " . $_FILES["file"]["error"] . "<br>"; }
  else { 
  
  if (! file_exists("profiles/" . $_SESSION['Username'] . "/" . $_FILES["file"]["name"]))
     { move_uploaded_file($_FILES["file"]["tmp_name"], "avatars/" . $_SESSION['Username'] . "/" . $_FILES["file"]["name"]); }
		// either file is new, then move file, else file previously uploaded: simply only change the name
		
		$con = mysql_connect($SQLhost, $SQLuser, $SQLpass) or die('Could not connect: ' . mysql_error()); 
		mysql_select_db($SQLdb, $con) or die('Could not find database: ' . mysql_error()) ; 
	
		$sql_pic=("UPDATE users SET Avatar = '".$_SESSION['Username'] . "/" . $_FILES["file"]["name"] ."' WHERE Username = '".$_SESSION['Username']."' ");
		
		if(!mysql_query($sql_pic, $con) ) { die ("Query error: ". mysql_error() ); }
		else { header('Location: users.php?u='.$_SESSION['Username']); }		
	} }
else // if not allowed file, or no file at all: return to previous page
  {   echo '<script>window.history.back()</script>'; }
?> 